
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class RectangleFrame extends JFrame{
    JLabel lblWide;
    JLabel lblHigh;
    JTextField txtWide;
    JTextField txtHigh;
    JButton btnCalculate;
    JLabel lblResult;

    /**
     *
     */
    public RectangleFrame() {
        super("Rectangle");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500, 300);
        this.setLayout(null);
        

        lblWide =new JLabel("Wide:",JLabel.TRAILING);
        lblWide.setSize(50, 20);
        lblWide.setLocation(5, 5);
        lblWide.setBackground(Color.WHITE);
        lblWide.setOpaque(true);
        this.add(lblWide);
        
        lblHigh =new JLabel("Height:",JLabel.TRAILING);
        lblHigh.setSize(50, 20);
        lblHigh.setLocation(5, 25);
        lblHigh.setBackground(Color.WHITE);
        lblHigh.setOpaque(true);
        this.add(lblHigh);
        
        txtWide =new JTextField();
        txtWide.setSize(50, 20);
        txtWide.setLocation(60, 5);
        this.add(txtWide);
        
        txtHigh =new JTextField();
        txtHigh.setSize(50, 20);
        txtHigh.setLocation(60, 25);
        this.add(txtHigh);
        
        btnCalculate =new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);
        
        lblResult =new JLabel("Rectangle radius= ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    String strWide = txtWide.getText();
                    String strHigh = txtHigh.getText();
                    double wide = Double.parseDouble(strWide);
                    double high = Double.parseDouble(strHigh);
                    Rectangle rectangle=new Rectangle(wide,high);
                    lblResult.setText(" Rectangle Wide = " + String.format("%.2f", rectangle.getWide())
                            + ", High = " + rectangle.getHigh()
                            + " Area = " + String.format("%.2f", rectangle.calArea())
                            + " Perimeter = " + String.format("%.2f", rectangle.calPerimeter()));

                }catch(Exception ex){
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtWide.setText("");
                    txtHigh.setText("");
                    txtWide.requestFocus();
                    txtHigh.requestFocus();
                    
                }
            }
        });
    }
    public static void main(String[] args) {
        RectangleFrame frame=new RectangleFrame();
        frame.setVisible(true);

    
}
}
