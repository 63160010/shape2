/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Rectangle extends Shape{
    private double wide;
    private double high;
    public Rectangle(double wide, double high) {
        super("Rectangle");
        this.wide = wide;
        this.high = high;
    }

    public double getWide() {
        return wide;
    }

    public void setWide(double wide) {
        this.wide = wide;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }
    @Override
    public double calArea() {
        return wide*high;
    }

    @Override
    public double calPerimeter() {
        return 2*(wide+high);
    }
}
